//
//  MessageToPolynomial.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-24.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PolynomialClass.h"

@interface MessageToPolynomial : NSObject

-(polinomio)convertMessage:(NSString *)message;
-(NSMutableArray *)fileToOperator:(NSString *)message:(int)dimension;

-(NSMutableArray *)MessageToOperator:(NSString *)message;
//-(NSMutableArray *)photoToOperator : (nsp)
@end
