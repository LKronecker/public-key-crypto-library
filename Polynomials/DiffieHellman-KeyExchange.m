//
//  DiffieHellman-KeyExchange.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DiffieHellman-KeyExchange.h"

@implementation DiffieHellman_KeyExchange

-(void) generateContext:(int)exp{
    FiniteFIeldsBase2 *keyArithmetics = [[FiniteFIeldsBase2 alloc]init];
   NSArray *irredArray = [keyArithmetics irreductiblePolynomials:exp];
   // NSLog(@"Irreductibles:%@", irredArray);
    
    polinomio irred = [[irredArray objectAtIndex:0] intValue];
    irreductible = irred;
    
    int partbase = pow(2, exp);
    polinomio base = partbase - exp;
    baseG = base;
    NSLog(@"Base G:%ld", base);
    [keyArithmetics release];
}

-(polinomio) createPublicKey:(polinomio)secKey{
     FiniteFIeldsBase2 *keyArithmetics = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *polyKey = [[PolynomialClass alloc]init];
    
    polinomio partKey = [keyArithmetics elevarGFbase2:baseG :secKey];
    NSLog(@"G^SK:%ld", partKey);
    polinomio pubKey = [polyKey residuo:partKey :irreductible];
    NSLog(@"%ld", pubKey);
    publicKey =pubKey;
        [keyArithmetics release];
    [polyKey release];
    return (pubKey);
}

-(polinomio) createSECRETkey:(polinomio)pubKey:(polinomio)secretKey{
    FiniteFIeldsBase2 *keyArithmetics = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *polyKey = [[PolynomialClass alloc]init];
    
    polinomio partKey = [keyArithmetics elevarGFbase2:pubKey:secretKey];
    NSLog(@"PK^SK:%ld", partKey);
    polinomio SECKey = [polyKey residuo:partKey :irreductible];
   // NSLog(@"%ld", SECKey);
    
    [keyArithmetics release];
    [polyKey release];
    return (SECKey);

}

@end
