//
//  lfsrCipher.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2013-05-27.
//
//

#import "lfsrCipher.h"

@implementation lfsrCipher

-(NSString *)CipherLFSR:(NSString *)message:(polinomio)key{
    NSMutableArray *chipheredArray = [[NSMutableArray alloc]init];
    NSMutableArray *lfsrArray = [[NSMutableArray alloc]init];
    NSMutableArray *messageArray = [[NSMutableArray alloc]init];
    
      NSMutableString *paddedM =[[NSMutableString alloc]init];
     MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];
    PolynomialClass *arth = [[PolynomialClass alloc]init];
    
    polinomio refKey = [arth residuo:key :4295000729L];
    messageArray = [convertFile MessageToOperator:message];
  //  NSLog(@"%@", messageArray);
    
    NSUInteger cnt = [messageArray count];
    
   // NSLog(@"%lu", cnt);
    
    LFSR *lfsr = [[LFSR alloc]init];
    int L=4,i;
    polinomio C[4],S[4];
    
    
 //   NSLog(@"%ld, %ld", C[0], S[0]);
   // NSLog(@"%lu, %lu", C[1], S[1]);
    
    for(i=2;i<6;i++)
    {
    //    C[i-2]=(polinomio)(i-1);
      //  S[i-2]=(polinomio) i;
        
        C[i-2]=(polinomio)(refKey+i);
       S[i-2]=(polinomio) (refKey-i);

        
    //      NSLog(@"%ld, %ld",C[0], S[0]);
      //  NSLog(@"%ld, %ld",C[1], S[1]);
        //NSLog(@"%ld, %ld",C[2], S[2]);
        
    }
    for(i=0;i<cnt;i++){
        //    printf("%u ",LFSR(C,S,L));
        polinomio ls = [lfsr LFSR:C :S :L];
             NSLog(@"%ld %u", ls, [[messageArray objectAtIndex:i]intValue] );
        
        NSString *coefMODstring = [NSString stringWithFormat:@"%ld", ls];
        
        
        [lfsrArray addObject:coefMODstring];
    }
    
   // NSLog(@"%@", lfsrArray);
    
    for(i=0;i<cnt;i++){
    
        polinomio coeff = [[messageArray objectAtIndex:i]intValue];
        polinomio lfsrPoly = [[lfsrArray objectAtIndex:i]intValue];
        
        polinomio cipheredPoly = coeff ^ lfsrPoly;
        
        polinomio c = cipheredPoly;
        
      //  NSLog(@"%lu ^ %lu =             %lu \n", coeff, lfsrPoly, cipheredPoly);
        
        NSString *chiperedCoeff = [NSString stringWithFormat:@"%ld", cipheredPoly];
        
        
        [chipheredArray addObject:chiperedCoeff];
        
        // char c = cipheredPoly;
        
        NSString *chipheredChar = [NSString stringWithFormat:@"%ld", c];
        
        [paddedM appendString:chipheredChar];
        
      
        
    }
    
    //  NSLog(@"%@", paddedM);
    
    NSString * paddedint = (NSString *)[paddedM intValue];
   
    return (paddedint);
    
}


-(char *)CipherLFSRchar:(void *)message:(polinomio)key:(int)cnt:(char *)ciphertext{
 //   NSMutableArray *chipheredArray = [[NSMutableArray alloc]init];
 //   NSMutableArray *lfsrArray = [[NSMutableArray alloc]init];
 //   NSMutableArray *messageArray = [[NSMutableArray alloc]init];
    
 //   NSMutableString *paddedM =[[NSMutableString alloc]init];
  //  MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];
    PolynomialClass *arth = [[PolynomialClass alloc]init];
    
    polinomio refKey = key;//[arth residuo:key :(polinomio)11];
 //   messageArray = [convertFile MessageToOperator:message];
    //  NSLog(@"%@", messageArray);
    
    //NSUInteger cnt = [messageArray count];
    
    // NSLog(@"%lu", cnt);
    
    LFSR *lfsr = [[LFSR alloc]init];
    int L=4,L1=5,L2=7,i;
    polinomio C[4],S[4],C1[5],S1[5],C2[7],S2[7];
    
    
    //   NSLog(@"%ld, %ld", C[0], S[0]);
    // NSLog(@"%lu, %lu", C[1], S[1]);
    
    for(i=2;i<6;i++)
    {
        //    C[i-2]=(polinomio)(i-1);
        //  S[i-2]=(polinomio) i;
        
        C[i-2]=(polinomio)(refKey^i);
        S[i-2]=(polinomio) (refKey|i);
        
        
        //      NSLog(@"%ld, %ld",C[0], S[0]);
        //  NSLog(@"%ld, %ld",C[1], S[1]);
        //NSLog(@"%ld, %ld",C[2], S[2]);
        
    }
    
    for(i=3;i<8;i++)
    {
        
        
        C1[i-3]=(polinomio)(refKey^i);
        S1[i-3]=(polinomio) (refKey|i);
        
        
        
    }
    
    
    for(i=4;i<11;i++)
    {
        
        
        C2[i-4]=(polinomio)(refKey^i);
        S2[i-4]=(polinomio) (refKey|i);
        
        
        
    }
    
    polinomio llave;

    //cnt tiene el número de bytes del string (=número de caracteres)
    //por lo pronto voy a suponer que cnt es múltiplo de sizeof(polinomio)
    polinomio prov; //aquí voy a guardar el i-ésimo polinomio en el mensaje
    
    polinomio ls,ls1,ls2;   //este para la salida del LFSR
    char polbytes=sizeof(polinomio); //no son muchos bytes por eso char
    //NSLog(@"sizeof(polinomio)=%d",polbytes);
    polinomio cifrado[cnt/polbytes];
    //char ciphertext[cnt];
    //char carxcar; //cada caracter de polinomio
    char * mensaje=message; //tratamos message como chars para leer de byte en byte
    for(i=0;i<cnt/polbytes;i++){
        //    printf("%u ",LFSR(C,S,L));
        
        ls = [lfsr LFSR:C :S :L]; //pido un elemento del LFSR
        ls1 = [lfsr LFSR:C1 :S1 :L1]; //pido un elemento del LFSR 1
        ls2 = [lfsr LFSR:C2 :S2 :L2]; //pido un elemento del LFSR 2
        
        NSLog(@"%u %u %u",ls,ls1,ls2);
        
        //multiplicamos las tres salidas para generar nuestra llave
        //y le sumamos key
        //ojo: será esta una buena función?
        //mas adelante podemos probar evaluando en algún polinomio en tres variables fijo
        
        llave=[arth residuo:[arth multiplicacion:ls:ls1]:(polinomio)11];
        llave=[arth residuo:[arth multiplicacion:llave:ls2]:(polinomio)11];
        llave=llave^key;
        NSLog(@"llave:%u",llave);
        //memcpy(&prov, message+i*polbytes, polbytes); //leo el i-ésimo polinomio de message
        prov=mensaje[i];
        //estamos listos para cifrar: ls^prov
        cifrado[i]=llave^prov;
        ciphertext[i]=cifrado[i];
        //NSLog(@"claro:%u",prov);
        //NSLog(@"cifrado:%u",ciphertext[i]);
        //NSLog(@"prov:%u %c %u %u %c",prov,prov,ls,cifrado[i],cifrado[i]);
        /*int j;
        for(j=0;j<polbytes;j++)
        {
            memcpy(&carxcar, cifrado+j, 1);
            ciphertext[i*polbytes+j]=carxcar;
        }*/

   //     NSLog(@"%ld %u", ls, [[messageArray objectAtIndex:i]intValue] );
        
   //     NSString *coefMODstring = [NSString stringWithFormat:@"%ld", ls];
        
        
     //   [lfsrArray addObject:coefMODstring];
    }
    
    // NSLog(@"%s", ciphertext);
    ciphertext=cifrado;
    return cifrado;
  /*  for(i=0;i<cnt;i++){
        
        polinomio coeff = [[messageArray objectAtIndex:i]intValue];
        polinomio lfsrPoly = [[lfsrArray objectAtIndex:i]intValue];
        
        polinomio cipheredPoly = coeff ^ lfsrPoly;
        
        polinomio c = cipheredPoly;
        
        //  NSLog(@"%lu ^ %lu =             %lu \n", coeff, lfsrPoly, cipheredPoly);
        
        NSString *chiperedCoeff = [NSString stringWithFormat:@"%ld", cipheredPoly];
        
        
        [chipheredArray addObject:chiperedCoeff];
        
        // char c = cipheredPoly;
        
        NSString *chipheredChar = [NSString stringWithFormat:@"%ld", c];
        
        [paddedM appendString:chipheredChar];
        
        
        
    }
    
    //  NSLog(@"%@", paddedM);
    
    NSString * paddedint = (NSString *)[paddedM intValue];
    
    return (paddedint);  */
    
}

-(void )CipherLFSRvoid:(char *)message :(polinomio)key :(int)cnt :(char *)ciphertext{
    //   NSMutableArray *chipheredArray = [[NSMutableArray alloc]init];
    //   NSMutableArray *lfsrArray = [[NSMutableArray alloc]init];
    //   NSMutableArray *messageArray = [[NSMutableArray alloc]init];
    
    //   NSMutableString *paddedM =[[NSMutableString alloc]init];
    MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];
    PolynomialClass *arth = [[PolynomialClass alloc]init];
    
    polinomio refKey = [arth residuo:key :4295000729L];
    //   messageArray = [convertFile MessageToOperator:message];
    //  NSLog(@"%@", messageArray);
    
    //NSUInteger cnt = [messageArray count];
    
    // NSLog(@"%lu", cnt);
    
    LFSR *lfsr = [[LFSR alloc]init];
    int L=4,L1=5,L2=7,i;
    polinomio C[4],S[4],C1[5],S1[5],C2[7],S2[7];
    
    
    //   NSLog(@"%ld, %ld", C[0], S[0]);
    // NSLog(@"%lu, %lu", C[1], S[1]);
    
    for(i=2;i<6;i++)
    {
        //    C[i-2]=(polinomio)(i-1);
        //  S[i-2]=(polinomio) i;
        
        C[i-2]=(polinomio)(refKey+i);
        S[i-2]=(polinomio) (refKey-i);
        
        
        //      NSLog(@"%ld, %ld",C[0], S[0]);
        //  NSLog(@"%ld, %ld",C[1], S[1]);
        //NSLog(@"%ld, %ld",C[2], S[2]);
        
    }
    
    for(i=3;i<8;i++)
    {
        
        
        C1[i-3]=(polinomio)(refKey+i);
        S1[i-3]=(polinomio) (refKey-i);
        
        
        
    }
    
    
    for(i=4;i<11;i++)
    {
        
        
        C2[i-4]=(polinomio)(refKey+i);
        S2[i-4]=(polinomio) (refKey-i);
        
        
        
    }

    polinomio llave;
    
    //cnt tiene el número de bytes del string (=número de caracteres)
    //por lo pronto voy a suponer que cnt es múltiplo de sizeof(polinomio)
    polinomio prov; //aquí voy a guardar el i-ésimo polinomio en el mensaje
    polinomio cifra;
    polinomio ls,ls1,ls2;   //este para la salida del LFSR
    char polbytes=sizeof(polinomio); //no son muchos bytes por eso char
    //NSLog(@"sizeof(polinomio)=%d",polbytes);
    char * mensaje=message; //para evitar rollos de little-endian/big-endian
                            //pues en teoría char no depende de la arquitectura
    //char * cipher=ciphertext; //lo mismo
    
    //char ciphertext[cnt];
    //char carxcar; //cada caracter de polinomio
    int j;
    for(i=0;i<cnt/polbytes;i++){
        //    printf("%u ",LFSR(C,S,L));
        
        ls = [lfsr LFSR:C :S :L]; //pido un elemento del LFSR
        ls1 = [lfsr LFSR:C1 :S1 :L1]; //pido un elemento del LFSR 1
        ls2 = [lfsr LFSR:C2 :S2 :L2]; //pido un elemento del LFSR 2
        
        //multiplicamos las tres salidas para generar nuestra llave
        //ojo: será esta una buena función?
        //mas adelante podemos probar evaluando en algún polinomio en tres variables fijo
        
        llave=[arth residuo:[arth multiplicacion:ls:ls1]:4295000729L];
        llave=[arth residuo:[arth multiplicacion:llave:ls2]:4295000729L];
        
        NSLog(@"llave:%lu",llave);
        
        //memcpy(&prov, mensaje+i, polbytes); //leo el i-ésimo polinomio de message
                                              //ya no usamos memcpy
        //prov=mensaje[i];
        //estamos listos para cifrar: ls^prov
        
        //ciphertext[i]=cifrado[i];
        //NSLog(@"prov:%lu %lu %lu",prov,ls,ciphertext[i]);
        
        //el choro de abajo es para evitar usar el memcpy
        //da lata con el big-endian/little-endian
        prov=0L;
         for(j=0;j<polbytes;j++)
         {
         //memcpy(&carxcar, cifrado+j, 1);
             prov=(prov<<8)^(mensaje[polbytes*i+j]); //construimos el entero (polinomio en nuestro caso)
                                            //con los polbytes i-ésimos
             NSLog(@"enC:%u",mensaje[polbytes*i+j]);
         //ciphertext[i*polbytes+j]=(polinomio)carxcar;
         }
        
        //ahora si ciframos
        cifra=llave^prov;
        //el choro de abajo es para descomponer cifra en polbytes chars
        
        for(j=1;j<=polbytes;j++)
        {
            //memcpy(&carxcar, cifrado+j, 1);
            //prov=(prov<<8)^();
            
            //descomponemos el entero cifra en polbytes chars
            //con los polbytes i-ésimos
            
            //ciphertext[i*polbytes+j-1]=(( cifra & (( (polinomio) 255L )<< (8*(polbytes-j)) )  )>>(8*(polbytes-j)));
                                            //ojo 255=2^8-1  (8 bits prendidos)
            ciphertext[i*polbytes+j-1]=(cifra>>(8*(polbytes-j)))&255;
            NSLog(@"cipher:%u",ciphertext[i*polbytes+j-1]);
        }
        
        //NSLog(@"prov:%lu %lu %lu",prov,ls,cifra);
        
        //     NSLog(@"%ld %u", ls, [[messageArray objectAtIndex:i]intValue] );
        
        //     NSString *coefMODstring = [NSString stringWithFormat:@"%ld", ls];
        
        
        //   [lfsrArray addObject:coefMODstring];
    }
    
    // NSLog(@"%s", ciphertext);
    //ciphertext=cifrado;
    return ;
    /*  for(i=0;i<cnt;i++){
     
     polinomio coeff = [[messageArray objectAtIndex:i]intValue];
     polinomio lfsrPoly = [[lfsrArray objectAtIndex:i]intValue];
     
     polinomio cipheredPoly = coeff ^ lfsrPoly;
     
     polinomio c = cipheredPoly;
     
     //  NSLog(@"%lu ^ %lu =             %lu \n", coeff, lfsrPoly, cipheredPoly);
     
     NSString *chiperedCoeff = [NSString stringWithFormat:@"%ld", cipheredPoly];
     
     
     [chipheredArray addObject:chiperedCoeff];
     
     // char c = cipheredPoly;
     
     NSString *chipheredChar = [NSString stringWithFormat:@"%ld", c];
     
     [paddedM appendString:chipheredChar];
     
     
     
     }
     
     //  NSLog(@"%@", paddedM);
     
     NSString * paddedint = (NSString *)[paddedM intValue];
     
     return (paddedint);  */
    
}

-(NSString *)DEcipherLFSR:(NSString *) ciMessage:(polinomio)key{
    
    
    lfsrCipher *cipher =[[lfsrCipher alloc]init];
    
   // int *ciphstring ;
    NSString *DEciphstring ;
    
    DEciphstring = [cipher CipherLFSR:ciMessage :key];
    
    
    return (DEciphstring);
    
  //  NSLog(@"%i", DEciphstring);
    
 /*   NSMutableArray *DEchipheredArray = [[NSMutableArray alloc]init];
    NSMutableArray *lfsrArray = [[NSMutableArray alloc]init];
    NSMutableArray *messageArray = [[NSMutableArray alloc]init];
    NSMutableString *paddedM =[[NSMutableString alloc]init];
    
    MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];
    
  //  PolynomialClass *arithm = [[PolynomialClass alloc]init];
    PolynomialClass *arth = [[PolynomialClass alloc]init];
    
    polinomio refKey = [arth residuo:key :131L];

    messageArray = [convertFile MessageToOperator:ciMessage];
      // NSLog(@"%@", messageArray);
    
    NSUInteger cnt = [messageArray count];
    
    // NSLog(@"%lu", cnt);
    
    LFSR *lfsr = [[LFSR alloc]init];
    int L=4,i;
    polinomio C[4],S[4];
    
    
    //   NSLog(@"%ld, %ld", C[0], S[0]);
    // NSLog(@"%lu, %lu", C[1], S[1]);
    
    for(i=2;i<6;i++)
    {
       //    C[i-2]=(polinomio)(i-1);
       //    S[i-2]=(polinomio) i;
        
        C[i-2]=(polinomio)(refKey+i);
        S[i-2]=(polinomio) (refKey-i);
        
        
        //      NSLog(@"%ld, %ld",C[0], S[0]);
        //  NSLog(@"%ld, %ld",C[1], S[1]);
        //NSLog(@"%ld, %ld",C[2], S[2]);
        
    }
    for(i=0;i<cnt;i++){
        //    printf("%u ",LFSR(C,S,L));
        polinomio ls = [lfsr LFSR:C :S :L];
        //     NSLog(@"%ld", ls );
        
        NSString *coefMODstring = [NSString stringWithFormat:@"%ld", ls];
        
        
        [lfsrArray addObject:coefMODstring];
    }
    
    //  NSLog(@"%@", lfsrArray);
    
    for(i=0;i<cnt;i++){
        
        polinomio coeff = [[messageArray objectAtIndex:i]intValue];
        polinomio lfsrPoly = [[lfsrArray objectAtIndex:i]intValue];
        
        
        polinomio cipheredPoly = coeff ^ lfsrPoly;
        
        char c = cipheredPoly;
        
     //   NSLog(@"%c ^ %ld = %c", coeff, lfsrPoly, c);
        
        NSString *chiperedCoeff = [NSString stringWithFormat:@"%ld", cipheredPoly];
        
        
        [DEchipheredArray addObject:chiperedCoeff];
        
        // char c = cipheredPoly;
        
        NSString *chipheredChar = [NSString stringWithFormat:@"%c", c];
        
        [paddedM appendString:chipheredChar];
        
        
        
    }
    
       NSLog(@"%@", paddedM);
    
    return (paddedM);
  */
    

}

@end
