//
//  LFSR.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2013-05-26.
//
//

#import "LFSR.h"
#import "PolynomialClass.h"

@implementation LFSR




//polinomio LFSR(polinomio * c,polinomio * s,int L)


-(polinomio)LFSR:(polinomio *)c:(polinomio *)s:(int)L
{
    PolynomialClass *arithm = [[PolynomialClass alloc]init];
    //polinomio irre=4295000729L;
    polinomio irre=(polinomio)11;
    
    int i;
    polinomio SpL,lrr;
    lrr=0L;
    SpL=s[L-1];
    for(i=L-1;i>0;i=i-1)
    {
        //lrr=suma(lrr,residuo(multiplicacion(s[i],c[i]),irred));
        
        lrr = [arithm suma: lrr: [arithm residuo:[arithm multiplicacion:s[i]:c[i]]:irre]];
        s[i]=s[i-1];
         
    }
    // printf("%u ", SpL);
    s[0]=lrr;
    return(SpL);
}



@end
