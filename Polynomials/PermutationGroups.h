//
//  PermutationGroups.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-08.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(Permutation)

- (NSArray *)allPermutations;

@end
