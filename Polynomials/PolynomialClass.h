//
//  PolynomialClass.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-04-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define NUMERODEBITS 8
typedef char polinomio;
@interface PolynomialClass : NSObject{
    long polinomEntero;
    NSString *polyString;
    int degree;
}
//-(void) getPoly: (long)p;
-(void) displayPoly;
-(int) setDegree : (polinomio)p;
-(NSString *) generatePoly: (polinomio)p;
-(polinomio) suma: (polinomio)p: (polinomio)q;
-(polinomio) multiplicacion: (polinomio)p: (polinomio)q;
-(polinomio) residuo: (polinomio)p: (polinomio)q;
-(void) evaluar: (polinomio)p: (int)x;

-(polinomio) integerRepresentation : (NSArray *)abstractPol;


@end
