//
//  PolynomialClass.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-04-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PolynomialClass.h"

@implementation PolynomialClass

-(void) displayPoly{
    NSLog(@"f(x) = %@. Grado %i, valor entero %ld", polyString, degree, polinomEntero);
     
}
-(NSString *) generatePoly: (polinomio)p{
    
    polinomEntero=p;
    ///Write Polinomial
    int i;
    NSMutableString* partialPoly = [NSMutableString string];
    if(p==0L) return(@"0");
    if(p & (1L<<0)) {
        NSString *pol = [NSString stringWithFormat:@"1 + "];
        [partialPoly appendString:pol];
    }
    if(p & (1L<<1)) {
        NSString *pol = [NSString stringWithFormat:@"x + "];
        [partialPoly appendString:pol];
    }


    for(i=2;i<NUMERODEBITS;i++)
    {
       
        if(p & (1L<<i)) {
        NSString *pol = [NSString stringWithFormat:@"x^%d +",i];
            [partialPoly appendString:pol];
    }
    }
        polyString = partialPoly;
    return (partialPoly);
    
    
    
}
-(int) setDegree : (polinomio)p{
    int j;
    NSMutableString* partialDeg = [NSMutableString string];
    for(j=1;j<(NUMERODEBITS);j++){
        if(p&(1L<<(NUMERODEBITS-j))){
            // NSLog(@"%i", (NUMERODEBITS-i));
            NSString *deg = [NSString stringWithFormat:@"%i, ",(NUMERODEBITS-j)];
            [partialDeg appendString:deg];
            
            }
        
        
        // degree = (NUMERODEBITS-i);
    }
    // NSLog(@"%@",partialDeg);
    NSScanner *scanner = [NSScanner scannerWithString:partialDeg];
    NSString *finalDegree = nil;
    [scanner scanUpToString:@"," intoString:&finalDegree];
    int degreePoly = [finalDegree intValue];
     degree = degreePoly; 
    return(degreePoly);
    
    
}


-(polinomio) suma: (polinomio)p: (polinomio)q{
    polinomio pMASq = p^q;
  //  [self writePoly:pMASq];
   // NSLog(@"%ld", pMASq);
    return (pMASq);

}

-(polinomio) multiplicacion: (polinomio)p: (polinomio)q{
    int i;
    int m = [self setDegree:q];
    polinomio prod=0L;
    
    for(i=0;i<=m+1;i++)
    {
        if(q&(1L<<(i))) prod= prod ^ (p)<<i;
    }
    //  NSLog(@"%ld", prod);
    return (prod);

}

-(void) evaluar: (polinomio)p: (int)x{
    int i,z=0,g;
    g=[self setDegree:p];
    for(i=0;i<=g;i++)
    {if(p&(1L<<(g-i))) z=z+ [self multiplicacion:x :g-i];}
   // return(z);
    NSLog(@"%i", z);

}

-(polinomio) residuo: (polinomio)p: (polinomio)q{
    int p0 = [self setDegree:p];
    int q0 = [self setDegree:q];
    if(p0<q0){
        [self generatePoly:p];
        return (p);}
    else {
    polinomio PentreQ = p^(q<<(p0-q0));
        return ([self residuo:PentreQ:q]);
    }
}

-(polinomio) integerRepresentation : (NSArray *)abstractPol{
    NSInteger dim = [abstractPol count];
    polinomio partial, final;
    polinomio part = 0;
    
    for (int i = 0; i<dim; i++) {
        polinomio coeff = [[abstractPol objectAtIndex:i] intValue];
        
        partial = (1L<<(coeff));
        part = partial ^ part;
    }
     final = part^1;
    
    return (final);
}

@end
