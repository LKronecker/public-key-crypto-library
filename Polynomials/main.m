//
//  main.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-04-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PolynomialClass.h"
#import "FiniteFIeldsBase2.h"
#import "PermutationGroups.h"
#import "NSSet+Combinatorics.h"
#import "PolynomialRingOverGL(2^n).h"
#import "DiffieHellman-KeyExchange.h"
#import "MessageToPolynomial.h"
#import "AES.h"
#import "Key-Expanssion.h"
#import "FirmasDigitalesUH.h"
#import "LFSR.h"
#import "lfsrCipher.h"
int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        lfsrCipher *cipher = [[lfsrCipher alloc]init];
        NSString * mesg=@"Retomando los esquemas de cifrado";
        
        //NSString * cif = [cipher CipherLFSR:mesg :45];
        
        //NSLog(@"Try:%@", cif ); //Octavio sigue dando lata
        
        //Objective C declara NSString de manera oscura
        //además codifica los símbolos imprimibles en UTF16 (tiene sus ventajas)
        //Tarea: aprender mas de la codificación/decodificación en UTF16
        //por lo pronto...
        //vamos a convertir la "cadena" a bytes (char *): OJO: no todos son imprimibles
        //y tenemos pedos con caracteres no standard, com la ñ, parece que no está en el ascii de ios
        const char * enC=[(NSString *)mesg UTF8String ];
        //y vemos que ya podemos tener acceso a los caracteres,
        //por ejemplo los primeros 3:
        //NSLog(@"%c %c %c",enC[0],enC[1],enC[2]);
        //y ahora toda:
        //NSLog(@"%s",enC);
        //nota que la ñ vale madres
        //acceso via polinomios
        //NSLog(@"enpoli:%u",((polinomio *)enC)[0]);
        //NSLog(@"enpoli:%u",((polinomio *)enC)[1]);
        //NSLog(@"enpoli:%u",((polinomio *)enC)[2]);
        //NSLog(@"enpoli:%u",((polinomio *)enC)[3]);
        ///////
        //Voy a declarar CipherLFSRchar en lfsrCipher.h y .m para no enredarme
        // y lo mando llamar
        //
        // tambien declaro version void
        
        NSUInteger cnt = [(NSString *)mesg length]; //truco para contar bytes?
        NSLog(@"cnt:%ld",(unsigned long)cnt);
        char ciphertext[cnt];
      //  enC=(char *)"Retomando los esquemas de cifrado";
        [cipher CipherLFSRchar: (char *)enC :4: (int)cnt:ciphertext];
        
        //aquí se nota lo que te decía cuando ciframos de polinomio en polinomio
        NSLog(@"Cifrado: %s",ciphertext);
        enC=ciphertext;
        [cipher CipherLFSRchar:(char *)enC :4:(int)cnt:ciphertext];
        NSLog(@"descifrado: %s",ciphertext);
        
        int o;
        for (o=0; o<cnt+1; o++) {
            NSLog(@"%c",ciphertext[o]);
        }
                //NSLog(@"%@",(NSString *)ciphertext[0]);
        exit(0); //octavio
        
        
        
               PolynomialClass *polinomioP = [[PolynomialClass alloc]init];
        NSLog(@"Dame un polinomio p");
        polinomio p;
        scanf("%li", &p);
        
        [polinomioP generatePoly:p];
        [polinomioP setDegree:p];
        [polinomioP displayPoly];
        
     /*   
        PolynomialClass *polinomioQ = [[PolynomialClass alloc]init];
        NSLog(@"Dame otro polinomio q");
        polinomio q;
        scanf("%i", &q);
        
        [polinomioQ generatePoly:q];
        [polinomioQ setDegree:q];
        [polinomioQ displayPoly];
        [polinomioQ release]; */

        
        FiniteFIeldsBase2 *Orden2n = [[FiniteFIeldsBase2 alloc]init];
        NSLog(@"/////Dame un exponente n para generar el campo 2^n\\\\");
        int e;
        scanf("%i", &e);
         
        [Orden2n generateField:e];
        [Orden2n displayField];
        NSLog(@"Sus elementos irreducibles;");
        NSArray *irredArray = [[NSArray alloc]initWithArray:[Orden2n irreductiblePolynomials:e]];
    
        for (int i =0; i < [irredArray count]; i++) {
            NSLog(@"%@", [irredArray objectAtIndex:i]);
        }
        
              
        
              /*      
        NSLog(@"Suma de p mas q en GF(2^n)");
        PolynomialClass *polinomioPmasQmod = [[PolynomialClass alloc]init];
        polinomio PmasQmod = [Orden2n sumaGFbase2:p :q];
        NSString *sumaMOD = [polinomioPmasQmod generatePoly:PmasQmod];
        NSLog(@"%ld: %@", PmasQmod, sumaMOD);
        NSLog(@"Producto de p y q en GF(2^n)");
        
        PolynomialClass *polinomioPporQmod = [[PolynomialClass alloc]init];
        polinomio PporQmod = [Orden2n productoGFbase2:p :q];
        NSString *prodMOD = [polinomioPporQmod generatePoly:PporQmod];
        NSLog(@"%ld: %@", PporQmod, prodMOD); 
        
        int exp = 4;
        NSLog(@"elevar p a la %i", exp);
        polinomio elev = [Orden2n elevarGFbase2:p :exp];
       
        NSString *elevar = [polinomioP generatePoly:elev];

        NSLog(@"%ld: %@", elev, elevar); */
        NSLog(@"Dame un elemento en el Anillo GL(2^n)[Y] para evaluar p.");
        long long function;
        scanf("%lld", &function);
       
       PolynomialRingOverGL_2_n_ *polyRing = [[PolynomialRingOverGL_2_n_ alloc]init];
        // NSArray *elements = [polyRing generateRing:4];
    //  int k = [elements count];
       // NSLog(@"Orden de GL(2^n)[Y]: %i", k);
      //  NSLog(@"%@", [polyRing displayElment:function]);
      
        polinomio eval = [polyRing evluatePolyinOperator:p :function:16];
        NSString *evalAbs = [polinomioP generatePoly:eval];
        
        NSLog(@"Resultado de evaluar %ld en %lld; %ld :%@",p,function,eval, evalAbs);
        
        polinomio compressed = [Orden2n compressionEndomorfism:eval :8];
        NSString *compAbs = [polinomioP generatePoly:compressed];
        NSLog(@"Resultade de la primera compresion; %ld: %@", compressed, compAbs);
        
        polinomio secondEval = [polyRing ReedSolomonEval:compressed :function:8];
        NSString *evalAbsSecond = [polinomioP generatePoly:secondEval];
        NSLog(@"Resultado de evaluar %ld en %lld; %ld :%@",compressed,function,secondEval, evalAbsSecond);
               
         [polinomioP release];
        
        
        

        /*    
         /////// COMBINATIONS
         NSSet *combinations = [[NSSet alloc]initWithObjects:@"2",@"1",@"0",@"3", nil];
         
         NSSet *combinationSet = [combinations variationsWithRepetitionsOfSize:4 ];
         NSArray *var = [combinationSet allObjects];
         NSString *poly = [NSString stringWithFormat:@"%@",[var objectAtIndex:1]];
         for (int i = 6; i<[poly length]; i=i+7) {
         
         char firstVal = [poly characterAtIndex:i];
         NSLog(@"%c", firstVal);
         }
         
         
         
         /////////PERMUTATIOS
         NSArray *nElements = [[NSArray alloc]initWithObjects:@"1",@"2",@"0", nil]; 
         NSArray *permutations = [nElements allPermutations];
         
         for (int i = 0; i<[permutations count]; i++) {
         NSLog(@"%@",[permutations objectAtIndex:i]);
         }
         */
        

/*
       
      //  ////////////////////////////////////////////////// 
        
        
       PolynomialClass *AnilloModQ = [[PolynomialClass alloc]init];
        NSLog(@"////////Resultado de P mod Q\\\\\\\\\\\\");
        
        [AnilloModQ residuo:p :q];
        [AnilloModQ displayPoly]; 
        [AnilloModQ release];
        

         PolynomialClass *polinomioPmasQ = [[PolynomialClass alloc]init];
        polinomio pMasq = [polinomioPmasQ suma:p :q];
        NSLog(@"///////RESULTADO DE LA SUMA\\\\\\");
        [polinomioPmasQ generatePoly:pMasq];
        
        [polinomioPmasQ displayPoly];
        [polinomioPmasQ release];
        
        PolynomialClass *polinomioPXQ = [[PolynomialClass alloc]init];
        polinomio pXq = [polinomioPXQ multiplicacion:p :q ];
        NSLog(@"RESULTADO DEl Producto");
        [polinomioPXQ generatePoly:pXq];
        
        [polinomioPXQ displayPoly];
        [polinomioPXQ release];
        
        NSLog(@"Dame un entero para evaluar los polinomios");
        int c;
        scanf("%i", &c);
        [polinomioP evaluar:p :c]; 
        [polinomioP release]; */
        
        
        }
    return 0;
}

