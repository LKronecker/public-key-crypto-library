//
//  PolynomialRingOverGL(2^n).h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-09.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FiniteFIeldsBase2.h"
#import "NSSet+Combinatorics.h"
#import "Key-Expanssion.h"
#import "MessageToPolynomial.h"

@interface PolynomialRingOverGL_2_n_ : NSObject{
    NSArray *ringElements;
    int dimension;
    int order;
}
-(NSArray *) generateRing:(int)k;
-(NSString *) displayElment: (int)e;
-(polinomio) evaluateElement:(polinomio)p :(int)element;
-(polinomio) evluatePolyinOperator:(polinomio)elem:(NSString *)oper: (int)exp;
-(polinomio) ReedSolomonEval:(polinomio)elem:(NSString *)oper:(int)exp;
@end
