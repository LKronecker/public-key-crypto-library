//
//  FirmasDigitalesUH.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2012-11-11.
//
//

#import "FirmasDigitalesUH.h"


@implementation FirmasDigitalesUH

-(polinomio)SigningAlgorithm: (polinomio)key: (NSString *)message{
      PolynomialRingOverGL_2_n_ *polyRing = [[PolynomialRingOverGL_2_n_ alloc]init];
     FiniteFIeldsBase2 *Orden2n = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *arithmetics = [[PolynomialClass alloc]init];
   
  
    
    polinomio eval = [polyRing ReedSolomonEval:key :message:32];
   // NSString *evalAbs = [arthPol generatePoly:eval];
 //   NSLog(@"Resultado de evaluar %ld EN %@; %ld",key, message, eval);
    
       
    polinomio secondEval = [polyRing evluatePolyinOperator:eval :message:32];
  //  NSString *SecEvalAbs = [arthPol generatePoly:secondEval];
    
     //NSLog(@"Resultado de evaluar %ld EN %@; %ld :%@",eval, message, secondEval, SecEvalAbs);
    

    
    polinomio compressed = [Orden2n compressionEndomorfism:secondEval :16];
  //  NSString *compAbs = [arthPol generatePoly:compressed];
    //NSLog(@"Resultade de la primera compresion; %ld: %@", compressed, compAbs);
    
    //Agregar la v este fin!!!!!
    polinomio verif = [arithmetics residuo:key :299];
    
    polinomio final =  compressed ^ verif; //[arithmetics suma:compressed :verif];
           
    return (final);


}


-(BOOL)VerificatonAlgorithm: (polinomio)key: (NSString *)message:(polinomio)signature{
    
    polinomio sig = [self SigningAlgorithm:key :message];
    
    if(sig == signature) return (1);
    else return (0);

}

@end
