//
//  LFSR.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2013-05-26.
//
//

#import <Foundation/Foundation.h>
#import "PolynomialClass.h"

@interface LFSR : NSObject{
    polinomio irred;

}
-(polinomio)LFSR:(polinomio *)c:(polinomio *)s:(int)L;

@end
