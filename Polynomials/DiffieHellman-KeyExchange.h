//
//  DiffieHellman-KeyExchange.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"

@interface DiffieHellman_KeyExchange : NSObject{
    polinomio privateKey;
    polinomio publicKey;
    polinomio baseG;
    polinomio irreductible;
}
-(void) generateContext:(int)exp;
-(polinomio) createPublicKey:(polinomio)secKey;
-(polinomio) createSECRETkey:(polinomio)pubKey:(polinomio)secretKey;

@end
