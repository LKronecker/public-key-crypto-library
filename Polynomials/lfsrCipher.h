//
//  lfsrCipher.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2013-05-27.
//
//

#import <Foundation/Foundation.h>
#import "MessageToPolynomial.h"
#import "LFSR.h"

@interface lfsrCipher : NSObject{
    
}

-(NSString *)CipherLFSR:(NSString *)message:(polinomio)key;

-(char *)CipherLFSRchar:(void *)message:(polinomio)key:(int)cnt:(char *)ciphertext;

-(void )CipherLFSRvoid:(char *)message:(polinomio)key:(int)cnt:(char *)ciphertext;

-(NSString *)DEcipherLFSR:(NSString *)ciMessage:(polinomio)key;

@end
